const Path = require('path')
const parts = require('./webpack.config.parts')
const merge = require('webpack-merge')

const root = Path.resolve(__dirname, '..')

const PATHS = {
  app: Path.resolve(root, 'src'),
  build: Path.resolve(root, 'public'),
  root,
  static: Path.resolve(root, 'static'),
  template: Path.resolve(root, 'src/index.html'),
}

const CORE_CONFIG = merge([
  {
    entry: {
      main: [PATHS.app],
    },
    output: {
      crossOriginLoading: 'anonymous',
      devtoolModuleFilenameTemplate: 'webpack:///[resource-path]',
      filename: '[name].js',
      path: PATHS.build,
      publicPath: '/',
    },
  },
  parts.friendlyErrors(),
  parts.babelize({ include: PATHS.app }),
  parts.lintJS(),
  parts.loadFonts(),
  parts.loadImages({ include: PATHS.app }),
  parts.ignoreMomentLocales(),
  parts.html({ template: PATHS.template }),
  parts.copyStatic({ from: PATHS.static, to: PATHS.build }),
  parts.generateSourceMaps(),
  parts.useModuleLevelCache(),
])

const devConfig = () =>
  merge.smart([
    { mode: 'development' },
    CORE_CONFIG,
    parts.devServer({
      poll: process.env.POLL,
      port: 3000,
      proxy: {
        '/apigw/rtg/rest/euromillions': {
          target: 'https://www.fdj.fr',
          secure: false,
        },
      },
    }),
    parts.loadCSS(),
    parts.loadSASS(),
    parts.errorOverlay(),
  ])

const productionConfig = () =>
  merge.smart([
    {
      mode: 'production',
      optimization: {
        runtimeChunk: true,
        splitChunks: {
          cacheGroups: {
            styles: {
              name: 'styles',
              test: /\.css$/,
              chunks: 'all',
              enforce: true,
            },
          },
          chunks: 'all',
        },
      },
      output: { filename: '[name].[chunkhash:8].js' },
    },
    CORE_CONFIG,
    parts.cleanDist(),
    parts.extractCSS(),
    parts.extractSASS(),
    parts.generateSourceMaps('source-map'),
    parts.minifyAll(),
    parts.optimizeImages(),
    parts.offline({
      AppCache: true,
      excludes: ['**/.*', '**/*.map', '**/*.gz'],
      externals: ['/history', '/settings'],
    }),
  ])

module.exports = (env = process.env.NODE_ENV) =>
  env === 'production' ? productionConfig() : devConfig()
