import { hot } from 'react-hot-loader/root'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { updateGrid } from '../../actions'
import Grid from '../../presentation/Grid'
import * as gridTypes from '../../constants/gridTypes'

const mapStateToProps = (state) => ({
  numerals: state.grid.filter((element) => element.type === gridTypes.NUMERAL),
  stars: state.grid.filter((element) => element.type === gridTypes.STAR),
})

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ updateGrid }, dispatch)

export default hot(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Grid)
)
