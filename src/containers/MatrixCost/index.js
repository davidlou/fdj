import { hot } from 'react-hot-loader/root'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { func, bool, number, array, object } from 'prop-types'
import { fetchMatrixCost } from '../../actions'
import { getTotalCost } from '../../selectors'
import Cost from '../../presentation/Cost'
import * as gridTypes from '../../constants/gridTypes'

class MatrixCost extends Component {
  async componentDidMount() {
    const { fetchMatrix } = this.props
    await fetchMatrix()
  }

  render() {
    const {
      loading,
      totalCost: { value, currency },
    } = this.props

    if (loading) {
      return <div>Chargement ...</div>
    }

    return <Cost cost={value} currency={currency} />
  }
}

MatrixCost.propTypes = {
  fetchMatrix: func.isRequired,
  loading: bool.isRequired,
  numerals: number.isRequired,
  stars: number.isRequired,
  multiples: array,
  totalCost: object,
}

MatrixCost.defaultProps = {
  totalCost: {
    currency: 'EUR',
    value: 0,
  },
}

const mapStateToProps = (state) => ({
  loading: state.matrixCost.loading,
  multiples: state.matrixCost.multiples,
  numerals: state.grid.filter((element) => element.type === gridTypes.NUMERAL)
    .length,
  stars: state.grid.filter((element) => element.type === gridTypes.STAR).length,
  totalCost: getTotalCost(state),
})

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      fetchMatrix: fetchMatrixCost,
    },
    dispatch
  )

export default hot(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(MatrixCost)
)
