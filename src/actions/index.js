import * as types from '../constants/actionTypes'
import { API_GET_MATRIX } from '../constants/API'

export const updateGrid = (gridType, value) => ({
  type: types.UPDATE_GRID,
  gridType,
  value,
})

export const updateTotal = (cost) => ({
  type: types.UPDATE_TOTAL,
  cost,
})

export const requestMatrixCost = () => ({
  type: types.REQUEST_MATRIX_COST,
  loading: true,
})

export const requestMatrixCostSuccess = (multiples) => ({
  type: types.REQUEST_MATRIX_COST_SUCCESS,
  multiples,
  loading: false,
})

export const requestMatrixCostError = () => ({
  type: types.REQUEST_MATRIX_COST_ERROR,
  loading: false,
})

export const fetchMatrixCost = () => {
  return async (dispatch) => {
    dispatch(requestMatrixCost)
    try {
      const response = await fetch(API_GET_MATRIX)
      const matrix = await response.json()
      dispatch(requestMatrixCostSuccess(matrix.multiples))
    } catch (error) {
      console.log(`An error while fetching data: ${error.message}`)
      dispatch(requestMatrixCostError())
    }
  }
}
