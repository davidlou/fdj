import { UPDATE_GRID } from '../constants/actionTypes'
import { createGrid } from '../utils/grid'

const initialState = createGrid()

export default function grid(state = initialState, action) {
  switch (action.type) {
    case UPDATE_GRID:
      return state.map((grid) => {
        return grid.value === action.value && grid.type === action.gridType
          ? {
              ...grid,
              isActive: !grid.isActive,
            }
          : grid
      })
    default:
      return state
  }
}
