import { combineReducers } from 'redux'
import grid from './grid'
import matrixCost from './matrixCost'

export default combineReducers({
  grid,
  matrixCost,
})
