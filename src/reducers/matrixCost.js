import {
  REQUEST_MATRIX_COST,
  REQUEST_MATRIX_COST_SUCCESS,
  REQUEST_MATRIX_COST_ERROR,
} from '../constants/actionTypes'

const initialState = {
  loading: false,
  multiples: [],
  error: null,
}

export default function matrixCost(state = initialState, action) {
  switch (action.type) {
    case REQUEST_MATRIX_COST:
      return {
        ...state,
        loading: true,
      }
    case REQUEST_MATRIX_COST_SUCCESS:
      return {
        ...state,
        loading: false,
        multiples: action.multiples,
      }
    case REQUEST_MATRIX_COST_ERROR:
      return {
        ...state,
        loading: false,
      }
    default:
      return state
  }
}
