import { hot } from 'react-hot-loader/root'
import React from 'react'
import Grid from '../../containers/Grid'
import MatrixCost from '../../containers/MatrixCost'
import style from './style.scss'

const App = () => {
  return (
    <div className={style.container}>
      <div className={style.content}>
        <MatrixCost />
        <Grid />
      </div>
    </div>
  )
}

export default hot(App)
