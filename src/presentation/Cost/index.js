import { hot } from 'react-hot-loader/root'
import React from 'react'
import { number, string } from 'prop-types'
import style from './style.scss'

const Cost = ({ cost, currency }) => {
  const options = {
    style: 'currency',
    currency,
  }
  return (
    <div className={style.container}>
      <div>Grille</div>
      <div className={style.border}>
        Mise totale: {(cost / 100).toLocaleString(undefined, options)}
      </div>
    </div>
  )
}

Cost.propTypes = {
  cost: number.isRequired,
  currency: string.isRequired,
}

export default hot(Cost)
