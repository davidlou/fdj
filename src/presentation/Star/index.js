import { hot } from 'react-hot-loader/root'
import React from 'react'
import { number, bool, func } from 'prop-types'
import classNames from 'classnames/bind'
import StarSvg from '../../svg/Star'
import * as gridTypes from '../../constants/gridTypes'
import style from './style.scss'

const cx = classNames.bind(style)

const Star = ({ value, isActive = false, updateGrid }) => {
  const className = cx({
    text: true,
    isActive,
  })
  return (
    <div className={style.container}>
      <StarSvg
        className={style.svg}
        viewBox='0 0 24 24'
        width={41}
        height={41}
        fill={isActive ? '#fcb749' : '#ffffff'}
      />
      <span
        className={className}
        role='presentation'
        onClick={() => updateGrid(gridTypes.STAR, value)}
      >
        {value}
      </span>
    </div>
  )
}

Star.propTypes = {
  value: number.isRequired,
  isActive: bool,
  updateGrid: func,
}

export default hot(Star)
