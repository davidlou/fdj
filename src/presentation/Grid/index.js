import { hot } from 'react-hot-loader/root'
import React from 'react'
import { func, array } from 'prop-types'
import ListNumeral from '../ListNumeral'
import ListStar from '../ListStar'
import style from './style.scss'

const Grid = ({ numerals, stars, updateGrid }) => {
  return (
    <div className={style.container}>
      <ListNumeral numerals={numerals} updateGrid={updateGrid} />
      <ListStar stars={stars} updateGrid={updateGrid} />
    </div>
  )
}

Grid.propTypes = {
  numerals: array.isRequired,
  stars: array.isRequired,
  updateGrid: func.isRequired,
}

export default hot(Grid)
