import { hot } from 'react-hot-loader/root'
import React from 'react'
import { array, func } from 'prop-types'
import Star from '../Star'
import style from './style.scss'

const ListStar = ({ stars, updateGrid }) => {
  return (
    <div className={style.container}>
      {stars.map((star, index) => (
        <Star
          key={index}
          value={star.value}
          isActive={star.isActive}
          updateGrid={updateGrid}
        />
      ))}
    </div>
  )
}
ListStar.propTypes = {
  stars: array.isRequired,
  updateGrid: func.isRequired,
}

export default hot(ListStar)
