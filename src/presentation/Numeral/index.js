import { hot } from 'react-hot-loader/root'
import React from 'react'
import { number, bool, func } from 'prop-types'
import classNames from 'classnames/bind'
import * as gridTypes from '../../constants/gridTypes'
import style from './style.scss'

const cx = classNames.bind(style)

const Numeral = ({ value, isActive = false, updateGrid }) => {
  const className = cx({
    circle: true,
    isActive,
  })
  return (
    <div
      className={style.container}
      role='presentation'
      onClick={() => updateGrid(gridTypes.NUMERAL, value)}
    >
      <span className={className}>{value}</span>
    </div>
  )
}

Numeral.propTypes = {
  value: number.isRequired,
  isActive: bool,
  updateGrid: func.isRequired,
}

export default hot(Numeral)
