import { hot } from 'react-hot-loader/root'
import React from 'react'
import { array, func } from 'prop-types'
import Numeral from '../Numeral'
import style from './style.scss'

const ListNumeral = ({ numerals, updateGrid }) => {
  return (
    <div className={style.container}>
      {numerals.map((numeral, index) => (
        <Numeral
          key={index}
          value={numeral.value}
          isActive={numeral.isActive}
          updateGrid={updateGrid}
        />
      ))}
    </div>
  )
}
ListNumeral.propTypes = {
  numerals: array.isRequired,
  updateGrid: func.isRequired,
}

export default hot(ListNumeral)
