import { createSelector } from 'reselect'
import * as gridTypes from '../constants/gridTypes'

const getNumerals = (state) =>
  state.grid.filter(
    (element) => element.type === gridTypes.NUMERAL && element.isActive === true
  ).length

const getStars = (state) =>
  state.grid.filter(
    (element) => element.type === gridTypes.STAR && element.isActive === true
  ).length

const getMultiples = (state) => state.matrixCost.multiples

export const getTotalCost = createSelector(
  [getMultiples, getNumerals, getStars],
  (multiples, numerals, stars) => {
    const multiple = multiples.find(
      (element) =>
        element.pattern[0] === numerals && element.pattern[1] === stars
    )
    return multiple ? multiple.cost : undefined
  }
)
