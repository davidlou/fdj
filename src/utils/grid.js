import * as gridTypes from '../constants/gridTypes'

export const createGridObject = (size, type) =>
  new Array(size)
    .fill()
    .map((e, index) => ({ isActive: false, type, value: index + 1 }))

export const createGrid = (numbers = 50, stars = 12) => [
  ...createGridObject(numbers, gridTypes.NUMERAL),
  ...createGridObject(stars, gridTypes.STAR),
]
